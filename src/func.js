const getSum = (str1, str2) => {
  let value1;
  let value2;
  if(/^-?\d+$/.test(str1) || str1 == '' && typeof str1 == "string"){
    if(isNaN(parseInt(str1))){
      value1 = 0;
    }
    else{
      value1 = parseInt(str1);
    }
  }
  else{
    return false;
  }
  if(/^-?\d+$/.test(str2) || str2 == '' && typeof str1 == "string"){
    if(isNaN(parseInt(str2))){
      value2 = 0;
    }
    else{
      value2 = parseInt(str2);
    }
  }
  else{
    return false;
  }
  return String(value1 + value2);
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let post_count = listOfPosts.filter(item => item.author == authorName).length;
  let comment_count = 0;
  for (let index = 0; index < listOfPosts.length; index++) {
    if(listOfPosts[index].hasOwnProperty('comments')){
      comment_count += listOfPosts[index].comments.filter(item => item.author == authorName).length;
    }
  }
  return `Post:${post_count},comments:${comment_count}`;
};

const tickets=(people)=> {
  let change = 0;
  for (let index = 0; index < people.length; index++) {
    let money = parseInt(people[index]);
    if(money == 25)
    {
      change += money;
    }
    else{
      change -= (money - 25);
      if(change >= 0 ){
        change += money;
      }
      else{
        return 'NO';
      }
    }
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
